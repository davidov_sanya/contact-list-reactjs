import React from 'react';
import {connect} from 'react-redux'

import './App.css';

import ContactList from "./ContactList/ContactList";
import ContactChange from "./ContactChange/ContactChange";

import {fetchContacts, fetchLocalContacts} from "./redux/actions/app";


class App extends React.Component {

  componentDidMount() {
    const {fetchLocalContacts, fetchContacts} = this.props;
    if (localStorage.getItem('contacts')) {
      fetchLocalContacts()
    } else {
      fetchContacts()
    }
  }

  render() {
    const {contactChangeIndex} = this.props;

    return (
      <div className="app">
        {contactChangeIndex !== null ? (
          <ContactChange/>
        ) : (
          <ContactList/>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { contacts, contactChangeIndex} = state.app;
  return {
    contacts,
    contactChangeIndex
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchContacts: () => dispatch(fetchContacts()),
    fetchLocalContacts: () => dispatch(fetchLocalContacts())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
