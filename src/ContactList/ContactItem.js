import React from 'react'
import {connect} from "react-redux";

import {contactClick} from "../redux/actions/app";


class ContactItem extends React.Component {
  render() {
    const {
      contact, index,
      contactClick
    } = this.props;

    return (

      <tr
        onClick={ contactClick(index) }
      >
        <th scope="row">{contact.id}</th>
        <td>{contact.name}</td>
        <td>{contact.username}</td>
      </tr>

    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    contactClick: (index) => () => dispatch(contactClick(index))
  }
}

export default connect(null, mapDispatchToProps)(ContactItem)
