import React from 'react'
import {Table} from 'reactstrap'
import {connect} from 'react-redux';

import './ContactList.css'

import ContactItem from "./ContactItem"
import Loader from '../UI/Loader/Loader';

import {sortContacts} from '../redux/actions/contactList';


class ContactList extends React.Component {

  render() {
    const {
      loading,
      cls, contacts,
      sortContacts
    } = this.props;

    return (
      <div className="contact-list">
        <div>
          <h1>User List</h1>

          {loading ? (
            <Loader/>
          ) : (
            <Table hover size="sm">

              <thead>
              <tr>

                <th onClick={sortContacts('id')} >
                  {`${cls.id ? '↓' : '\u00A0'} Id`}
                </th>

                <th onClick={sortContacts('name')} >
                  {`${cls.name ? '↓' : '\u00A0'} Name`}
                </th>

                <th onClick={sortContacts('username')} >
                  {`${cls.username ? '↓' : '\u00A0'} Username`}
                </th>

              </tr>
              </thead>

              <tbody>
              {contacts.map((contact, index) => {

                return (
                  <ContactItem
                    key={`contact_${index}`}
                    index={index}
                    contact={contact}
                  />
                )
              })}
              </tbody>

            </Table>
          )}

        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const {contacts, cls, loading} = state.app;
  return {
    contacts,
    cls,
    loading
  }
}

function mapDispatchToProps(dispatch) {
  return {
    sortContacts: (sortByProperty) => () => dispatch(sortContacts(sortByProperty))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactList)
