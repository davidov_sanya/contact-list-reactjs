import React from 'react'
import './Textarea.css'

function isInvalid({valid, touched, shouldValidate}) {
    return !valid && touched && shouldValidate
}

const Textarea = props => {
    const cls = ['Textarea'];
    const htmlFor = `${Math.random()}`;

    if(isInvalid(props)) {
        cls.push('invalid')
    }

    return (
        <div className={cls.join(' ')}>

            <label htmlFor={htmlFor}>{props.label}</label>
            <textarea
                id={htmlFor}
                value={props.value}
                onChange={props.onChange}
            />

            {
                isInvalid(props)
                    ? <span>{props.errorMessage || 'Введите верное значение'}</span>
                    : null
            }

        </div>
    )
};

export default Textarea
