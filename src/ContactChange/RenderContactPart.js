import React from 'react';

import Input from "../UI/Input/Input";


class RenderContactPart extends React.Component {
  state = {
    isShow: false
  };

  toggle = () => {
    const {isShow} = this.state;

    this.setState({
      isShow: !isShow
    })
  };

  render() {
    const {
      isShow
    } = this.state;

    const {
      changeablePart, property, title,
      onChange,
      children
    } = this.props;

    return (
      <div>


        <div
          className={`toggle fas ${isShow ? 'fa-caret-down' : 'fa-caret-right'}`}
          onClick={this.toggle}
        >
          &nbsp;{title}
        </div>

        <div className={'nested'}>
          {isShow && (
            <>
              {Object.keys(changeablePart).map((item, i) => {

                // let disabled = false;
                // if (item === 'id' || item === 'favorite') {
                //   disabled = true;
                // }

                if (typeof changeablePart[item] === 'object') {
                  return null;
                }

                const label = item[0].toUpperCase() + item.slice(1);

                return (
                  <Input
                    // disabled={disabled}
                    key={`${item}_${i}`}
                    label={label}
                    onChange={onChange(property, item)}
                    value={changeablePart[item]}
                  />
                )
              })}
              {children}
            </>
          )}



        </div>

      </div>
    );
  }
}

export default RenderContactPart