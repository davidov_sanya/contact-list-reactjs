import React from 'react'
import './RenderAccountHistory.css'
import Input from "../../../UI/Input/Input";
import './RenderAccountHistory.css'

class RenderAccountHistory extends React.Component {
  state = {
    accountHistory: this.props.accountHistory
  };

  changeAccountHistory = (property) => (event) => {
    const accountHistory = {
      ...this.state.accountHistory,
      [property]: event.target.value
    };

    this.setState({
      accountHistory
    });

    this.props.changeAccountHistory(accountHistory);
  };


  render() {
    return (
      <div className="RenderAccountHistory">
        <h1>Account History</h1>

        {
          Object.keys(this.state.accountHistory).map((item, i) => {
            const label = item[0].toUpperCase() + item.slice(1);
            const keyId = `${this.props.keyAccountStories}_accountHistory_${i}`;
            return (
              <Input
                key={keyId}
                label={label}
                value={this.state.accountHistory[item]}
                onChange={this.changeAccountHistory(item)}
              />
            )
          })
        }

      </div>
    )
  }
}

export default RenderAccountHistory
