import React from 'react'
import './RenderPost.css'
import Input from "../../../UI/Input/Input";
import Textarea from "../../../UI/Textarea/Textarea";

class RenderPost extends React.Component {
  state = {
    post: this.props.post,
  };

  changePost = (property) => (event) => {
    const changeablePart = {
      ...this.state.post,
      [property]: event.target.value
    };

    const post = {
      ...changeablePart,
      ...this.state.words
    };

    this.setState({
      post
    });

    this.props.changePost(post)
  };

  changeWords = (item) => (event) => {
    const changeablePart = [...this.state.post.words];
    changeablePart[item] = event.target.value;
    const post = {
      ...this.state.post,
      words: changeablePart
    };

    this.setState({
      post
    });
    this.props.changePost(post)
  };

  render() {
    return (
      <div className="RenderPost">
        <h1>Post</h1>
        <p>Words</p>
        {this.state.post.words.map((key, i) => {
          const keyId = `${this.props.keyPost}_words_${i}`;
          return (
            <Input
              key={keyId}
              value={this.state.post.words[i]}
              onChange={this.changeWords(i)}
            />
          )
        })}

        <Textarea
          label="Sentence"
          value={this.state.post.sentence}
          onChange={this.changePost('sentence')}
        />

        <Textarea
          label="Sentences"
          value={this.state.post.sentences}
          onChange={this.changePost('sentences')}
        />

        <Textarea
          label="Paragraph"
          value={this.state.post.paragraph}
          onChange={this.changePost('paragraph')}
        />
      </div>
    )
  }
}

export default RenderPost
