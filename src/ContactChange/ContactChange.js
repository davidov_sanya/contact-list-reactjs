import React from 'react'
import {connect} from 'react-redux';

import './ContactChange.css'

import RenderContact from './RenderContact';
import RenderContactPart from './RenderContactPart';
import RenderPost from './RenderContact/RenderPost/RenderPost';
import RenderAccountHistory from './RenderContact/RenderAccountHistory/RenderAccountHistory';

import {cancelClick, changeClick} from '../redux/actions/renderContact';


class ContactChange extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      contact: this.props.contact,
      address: this.props.contact.address,
      geo: this.props.contact.address.geo,
      company: this.props.contact.company,
      posts: this.props.contact.posts,
      // words: this.props.contact.posts.words,
      accountHistory: this.props.contact.accountHistory
    };
  }


  submitHandler = event => {
    event.preventDefault();
  };

  fillContact = () => {
    const {
      contact, address, geo, company, posts, accountHistory
    } = this.state;

    this.props.changeClick({
      ...contact, address, geo, company, posts, accountHistory
    });
  };

  changed = (property, item) => (event) => {
    this.setState({
      [property]: {
        ...this.state[property],
        [item]: event.target.value
      }
    });
  };

  changePost = (indexPost) => (post) => {
    const posts = [...this.state.posts];
    posts[indexPost] = post;

    this.setState({
      posts
    });
  };

  changeAccountHistory = (indexAccountHistory) => (accountHistory) => {
    const stories = [...this.state.accountHistory];
    stories[indexAccountHistory] = accountHistory;

    this.setState({
      accountHistory: stories
    });

  };


  render() {
    const {
      contact, address, geo, company, posts, accountHistory
    } = this.state;

    return (
      <div className="contact-change">

        <form onSubmit={this.submitHandler} className="contact-change_form">

          <button
            className='fa fa-times close'
            onClick={this.props.cancelClick}
          />
          <h1>Change contact</h1>

          <hr/>
          <RenderContact
            contact={contact}
            onChange={this.changed}
          />

          <hr/>
          <RenderContactPart
            title="Address"
            property="address"
            changeablePart={address}
            onChange={this.changed}
          >
            <RenderContactPart
              title="Geo"
              property="geo"
              changeablePart={geo}
              onChange={this.changed}
            />
          </RenderContactPart>

          <hr/>
          <RenderContactPart
            title="Company"
            property="company"
            changeablePart={company}
            onChange={this.changed}
          />

          <hr/>
          <RenderContactPart
            title="Posts"
            property="posts"
            changeablePart={posts}
            onChange={this.changed}
          >
            {posts.map((post, i) => {
              return (
                <RenderPost
                  key={`${post}_${i}`}
                  keyPost={`post_${i}`}
                  post={post}
                  changePost={this.changePost(i)}
                />
              )
            })}
          </RenderContactPart>

          <hr/>
          <RenderContactPart
            title="AccountHistory"
            property="accountHistory"
            changeablePart={accountHistory}
            onChange={this.changed}
          >
            {accountHistory.map((key, i) => {
              return (
                <RenderAccountHistory
                  key={`${key}_${i}`}
                  keyAccountStories={`stories_${i}`}
                  accountHistory={key}
                  changeAccountHistory={this.changeAccountHistory(i)}
                />
              )
            })}
          </RenderContactPart>


          <div style={{display: "flex", justifyContent: "flex-end", marginTop: "30px"}}>
            <button
              className='change'
              onClick={this.fillContact}
            >
              Change
            </button>

            <button
              className='cancel'
              onClick={this.props.cancelClick}
            >
              Cancel
            </button>
          </div>

        </form>
      </div>

    )
  }
}

function mapStateToProps(state) {

  return {
    contact: state.app.contacts[state.app.contactChangeIndex],
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeClick: (contact) => dispatch(changeClick(contact)),
    cancelClick: () => dispatch(cancelClick())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ContactChange)
