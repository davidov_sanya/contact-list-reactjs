import React from 'react'

import Input from '../UI/Input/Input';


class RenderContact extends React.Component {

  renderInput = (property) => {

    const {
      contact,
      onChange
    } = this.props;

    let disabled = false;
    if (property === 'id' || property === 'favorite') {
      disabled = true;
    }

    const label = property[0].toUpperCase() + property.slice(1);

    return (
      <Input
        disabled={disabled}
        label={label}
        onChange={onChange('contact', property)}
        value={contact[property]}
      />
    )

  };


  render() {

    return (
      <>
        {this.renderInput('name')}
        {this.renderInput('username')}
        {this.renderInput('phone')}
        {this.renderInput('email')}
        {this.renderInput('website')}
        {this.renderInput('avatar')}
        {this.renderInput('id')}
        {this.renderInput('favorite')}
      </>
    )
  }
}

export default RenderContact
