import {
  CANCEL_CLICK,
  CHANGE_CLICK,
  CONTACT_CLICK,
  FETCH_CONTACTS_ERROR,
  FETCH_CONTACTS_START,
  FETCH_CONTACTS_SUCCESS, FETCH_LOCAL_CONTACTS, SORT_CONTACTS
} from "../actions/actionTypes";

const initialState = {
  contacts: [],
  contactChangeIndex: null,
  loading: false,
  cls: {
    id: true,
    name: false,
    username: false
  }
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {

    case FETCH_CONTACTS_START:
      return {
        ...state,
        loading: true
      };

    case FETCH_CONTACTS_SUCCESS:
      return {
        ...state,
        loading: false,
        contacts: action.contacts
      };

    case FETCH_CONTACTS_ERROR:
      return {
        ...state,
        loading: false
      };

    case FETCH_LOCAL_CONTACTS:
      return {
        ...state,
        contacts: action.contacts
      };

    case CONTACT_CLICK:
      return {
        ...state,
        contactChangeIndex: action.payload
      };

    case CHANGE_CLICK:
      let contacts = [...state.contacts];
      contacts[state.contactChangeIndex] = {...action.payload};
      localStorage.setItem('contacts', JSON.stringify(contacts));
      return {
        ...state,
        contacts,
        contactChangeIndex: null
      };

    case CANCEL_CLICK:
      return {
        ...state,
        contactChangeIndex: null
      };

    case SORT_CONTACTS:
        let sortContacts = [...state.contacts];
        let cls = {
          id: false,
          name: false,
          username: false
        };
        cls[action.payload] = true;

        sortContacts.sort((a, b) => {
          return (a[action.payload] > b[action.payload] ? 1 : -1)
        });
      return {
        ...state,
        contacts: [...sortContacts],
        cls: cls
      };

    default:
      return state
  }
}