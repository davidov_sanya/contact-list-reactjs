import {SORT_CONTACTS} from "./actionTypes";

export function sortContacts(sortByProperty) {
  return {
    type: SORT_CONTACTS,
    payload: sortByProperty
  }
}