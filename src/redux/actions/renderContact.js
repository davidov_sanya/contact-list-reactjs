import {CANCEL_CLICK, CHANGE_CLICK, CHANGED} from "./actionTypes";

export function changeClick(contact) {
  return {
    type: CHANGE_CLICK,
    payload: contact
  }
}

export function cancelClick() {
  return {
    type: CANCEL_CLICK
  }
}

export function changed() {
  return {
    type: CHANGED,
    payload: {
    }
  }
}
