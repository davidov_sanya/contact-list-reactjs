import axios from 'axios'
import {
  CONTACT_CLICK,
  FETCH_CONTACTS_ERROR,
  FETCH_CONTACTS_START,
  FETCH_CONTACTS_SUCCESS, FETCH_LOCAL_CONTACTS
} from "./actionTypes";


export function fetchContacts() {
  return async dispatch => {
    dispatch(fetchContactsStart());
    try {
      const response = await axios.get('https://contactlist-6b962.firebaseio.com/.json');
      const contacts = [...response.data];
      dispatch(fetchContactsSuccess(contacts));

    } catch (e) {
      dispatch(fetchContactsError(e))
    }
  }
}

export function fetchContactsStart() {
  return {
    type: FETCH_CONTACTS_START
  }
}

export function fetchContactsSuccess(contacts) {
  return {
    type: FETCH_CONTACTS_SUCCESS,
    contacts
  }
}

export function fetchContactsError(error) {
  return {
    type: FETCH_CONTACTS_ERROR,
    error
  }
}

export function fetchLocalContacts() {
  const contacts = JSON.parse(localStorage.getItem('contacts'));
  return {
    type: FETCH_LOCAL_CONTACTS,
    contacts
  }
}

export function contactClick(index) {
  return {
    type: CONTACT_CLICK,
    payload: index
  }
}

